let mysql = require("async-mysql")
let con = require("../config/db")
let bcrypt = require('bcrypt')


async function getCrs(req, res) {
    let body = '';
    req.on('data', (dt)=>{
        body+= dt
    })

    req.on('end', async() => {
        
    res.writeHead(200, {"Content-Type":"application/json;charset=utf-8"})

        body = JSON.parse(body)
        

        let {name} = body;

        let query = 
            `select c.name cName, f.name fName
            from course_std cs, courses c, facs f
            WHERE
            cs.std_id = (SELECT id from users where users.name = "${name}") AND
            f.id = (SELECT id from users where users.fac_id = "${name}") and
            cs.crs_id = c.id AND
            c.fac_id = f.id
            `;
        courses = await (await con()).query(query);

        courses = (JSON.stringify(courses))

        if(courses) res.end(courses)
        else {
            res.writeHead(200, {"Content-Type":"text/plain;charset=utf-8"})
            res.end("no courses")
        }
    })
   
    
}

async function joinCrs(req, res) {

    let body = '';
    req.on('data', (dt)=>{
        body+= dt
    })

    req.on('end', async() => {

        res.writeHead(200, {"Content-Type":"text/plain;charset=utf-8"})

        body = JSON.parse(body)

        let { name, courses = [] } = body;

        let query = `
            SELECT COUNT(id) from course_std
            where course_std.std_id = (SELECT id from users where name = "${name}")`;

        coursesCount = await (await con()).query(query);
        coursesCount = Object.values(JSON.parse(JSON.stringify(coursesCount))[0])[0];

        if(coursesCount == 5)
            return res.end("you have the maximum count of courses !");

        let mustDelete = courses.length - (5-coursesCount);

        if (mustDelete > 0) 
            return res.end(`you have to delete ${mustDelete} course${(mustDelete != 1)?'s':''} !`);

        query = `select id from users where name = '${name}'`;
        user = await (await con()).query(query);
        user = JSON.parse(JSON.stringify(user))[0]
        let userId = user['id'];

        courses = courses.map(courseId => [userId, courseId]);

        query = `insert into course_std (std_id,crs_id) values ?`;
        courses = await (await con()).query(query, [courses]);

        return res.end(" registered succesfully");
    })
   
    
}

exports.getCrs = getCrs;
exports.joinCrs = joinCrs;