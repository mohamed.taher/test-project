let mysql = require("async-mysql")
let con = require("../config/db")
let bcrypt = require('bcrypt')


async function reg(req, res) {

    let body = '';
    req.on('data', (dt)=>{
        body+= dt
    })

    req.on('end', async() => {

        res.writeHead(200, {"Content-Type":"text/plain;charset=utf-8"})

        body = JSON.parse(body)

        let { name, password, fac_id = 0 } = body;

        if (name.length < 5 || password < 5) return res.end("Error .. name or password");

        const salt = await bcrypt.genSalt(10);
        password = await bcrypt.hash(password, salt);

        let query = `insert into users (name,password,role,fac_id) values('${name}', '${password}', 'std','${fac_id}')`;
        user = await (await con()).query(query);

        return res.end(name + " registered succesfully");
    })
   
    
}

async function log(req, res) {
    let body = '';
    req.on('data', (dt)=>{
        body+= dt
    })

    req.on('end', async() => {
        
    res.writeHead(200, {"Content-Type":"application/json;charset=utf-8"})

        body = JSON.parse(body)
        

        let { name, password} = body;
        
        let query = `select * from users where name = '${name}'`;
        user = await (await con()).query(query);
        user = JSON.parse(JSON.stringify(user))[0]

        userPassword = user['password'];

        const validPassword = await bcrypt.compare(password, userPassword);

        if(validPassword) res.end(JSON.stringify(user));
        else {
            res.writeHead(200, {"Content-Type":"text/plain;charset=utf-8"})
            res.end("invalid user")
        }
    })
   
    
}


exports.reg = reg;
exports.log = log;

