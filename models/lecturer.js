let mysql = require("async-mysql")
let con = require("../config/db")
let bcrypt = require('bcrypt')

async function adLec(req, res) {

    let body = '';
    req.on('data', (dt)=>{
        body+= dt
    })

    req.on('end', async() => {

        res.writeHead(200, {"Content-Type":"text/plain;charset=utf-8"})

        body = JSON.parse(body)

        let { name, password, fac_id = 0, admin = true } = body;
        
        if (! admin) return res.end("you are not authonticated to do this");


        if (name.length < 5 || password < 5) return res.end("Error .. name or password");

        const salt = await bcrypt.genSalt(10);
        password = await bcrypt.hash(password, salt);

        let query = `insert into users (name,password,role,fac_id) values('${name}', '${password}', 'lec','${fac_id}')`;
        user = await (await con()).query(query);

        return res.end(name + " registered succesfully");
    })
   
    
}

async function getStds(req, res) {
    let body = '';
    req.on('data', (dt)=>{
        body+= dt
    })

    req.on('end', async() => {
        
    res.writeHead(200, {"Content-Type":"application/json;charset=utf-8"})

        body = JSON.parse(body)
        

        let {name} = body;

        let query = 
            `SELECT DISTINCT std.name
            from course_std cs, courses c, users std, users lec
            WHERE
            lec.id = (SELECT id from users where name = "${name}") and
            lec.id = c.lec_id and
            std.id = cs.std_id
            `;
        students = await (await con()).query(query);

        students = (JSON.stringify(students))

        if(students) res.end(students)
        else {
            res.writeHead(200, {"Content-Type":"text/plain;charset=utf-8"})
            res.end("no courses")
        }
    })
   
    
}

exports.ad = adLec;
exports.getStds = getStds;