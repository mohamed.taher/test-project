let { reg, log } = require('../models/user.js'),
    {http} = require("../config/routes")


let register = async(req, res) => {

    http(req, res, "post")  //request type

    reg(req,res)
    
}

let login = async(req, res) => {

    http(req, res, "post")
    log(req, res)
}


exports.register = register;
exports.login = login;