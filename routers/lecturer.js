let {ad,getStds} = require('../models/lecturer.js'),
    {http} = require("../config/routes")



let addLecturer = async(req, res) => {

    http(req, res, "post")  //request type

    ad(req,res)
    
}

let getStudents = async(req, res) => {

    http(req, res, "post")  //request type

    getStds(req,res)
    
}

exports.addLecturer = addLecturer;
exports.getStudents = getStudents;