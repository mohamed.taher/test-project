let {getCrs, joinCrs} = require('../models/student.js'),
    {http} = require("../config/routes")


let getCourses = async(req, res) => {

    http(req, res, "post")
    getCrs(req, res)
}


let joinCourses = async(req, res) => {

    http(req, res, "post")
    joinCrs(req, res)
}

exports.getCourses = getCourses;
exports.joinCourses = joinCourses;


