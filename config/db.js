let mysql = require("async-mysql")


let con = async () => {
    let conn = await mysql.connect({
        host: "localhost",
        user: "root",
        password: "",
        database: "my_db"
    });
    return conn
}

module.exports = con;