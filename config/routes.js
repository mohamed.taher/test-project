let url = require('url')


let routes = {
    "/api/users/": 'user',
    '/api/students/': 'student',
    '/api/lecturers/': 'lecturer',
    '/api/courses/': 'course'
};

function contain(arr, str){
    for(elm in arr) if(str.indexOf(arr[elm])==0) return elm;return -1;
}

function requestType(req, res, method) {
    if(method.toUpperCase() != req.method.toUpperCase())
        return res.end("HTTP Connection Error !")    
}

exports.process = async(req, res) => {
    let path = url.parse(req.url).path,
        ind = contain(Object.keys(routes),path);

        if(ind == -1) return

        routerPath = "../routers/" + Object.values(routes)[ind] + ".js";

        let routerObj = require(routerPath);

        funcPath = path.replace(Object.keys(routes)[ind],'')
    
        func = await routerObj[funcPath]

        await func(req,res)


}

exports.http = requestType;

