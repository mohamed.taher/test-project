let http = require('http')
let router = require('./config/routes')

let host = 'localhost';
let port = 7777;


http.createServer((req, res) => {

    router.process(req, res)

})
.listen(port,host);


console.log(`listening to http://${host}:${port}`)