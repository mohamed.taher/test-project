-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2019 at 10:05 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(200) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `fac_id` int(200) NOT NULL,
  `lec_id` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `name`, `fac_id`, `lec_id`) VALUES
(1, 'eng', 1, 1),
(2, 'arabic', 1, 61),
(3, 'math', 1, 62),
(4, 'math6', 1, 62);

-- --------------------------------------------------------

--
-- Table structure for table `course_std`
--

CREATE TABLE `course_std` (
  `id` int(11) NOT NULL,
  `std_id` int(11) NOT NULL COMMENT 'user-student-id',
  `crs_id` int(11) NOT NULL COMMENT 'course-id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `course_std`
--

INSERT INTO `course_std` (`id`, `std_id`, `crs_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(10, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `facs`
--

CREATE TABLE `facs` (
  `id` int(200) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `facs`
--

INSERT INTO `facs` (`id`, `name`) VALUES
(0, ''),
(1, 'tanta_faculty'),
(2, 'cairo_faculty');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `role` text COLLATE utf8_unicode_ci NOT NULL,
  `fac_id` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `role`, `fac_id`) VALUES
(1, 'admin', '$2b$10$.QCAXZd1yTd2WN9yT6GMH.91X7RgKN0I/jVD/UXHXFTAUg5DBiGAu', 'adm', 0),
(2, 'modaa', '$2b$10$.QCAXZd1yTd2WN9yT6GMH.91X7RgKN0I/jVD/UXHXFTAUg5DBiGAu', 'std', 1),
(61, 'ahmed', '$2b$10$.QCAXZd1yTd2WN9yT6GMH.91X7RgKN0I/jVD/UXHXFTAUg5DBiGAu', 'lec', 1),
(62, 'amer', '$2b$10$.QCAXZd1yTd2WN9yT6GMH.91X7RgKN0I/jVD/UXHXFTAUg5DBiGAu', 'lec', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_std`
--
ALTER TABLE `course_std`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facs`
--
ALTER TABLE `facs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `course_std`
--
ALTER TABLE `course_std`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `facs`
--
ALTER TABLE `facs`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
